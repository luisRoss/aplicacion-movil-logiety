package com.logiety.app.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.logiety.app.bean.Contenedor;
import com.logiety.app.bean.Fraccion;
import com.logiety.app.fragment.MostrarContenedorFragment;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.Tools;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;


public class AceptarContenedorAsyncTask extends AsyncTask<String, Integer, Boolean> {
    private String cadenaConexion;
    private View vista;
    private MostrarContenedorFragment fragment;
    private Context contexto;

    private String mensajeError;
    private Contenedor idContenedor;


    public AceptarContenedorAsyncTask(MostrarContenedorFragment fragment, View vista, Context contexto,
                                      String cadenaConexion, Contenedor idContenedor) {
        this.cadenaConexion = cadenaConexion;
        this.fragment = fragment;
        this.vista = vista;
        this.idContenedor = idContenedor;
        this.contexto = contexto;
        this.mensajeError = "";
    }

    protected Boolean doInBackground(String... params) {

        try {
            HashMap<String, String> parametros = new HashMap<>();
            parametros.put("cadena", cadenaConexion);
            parametros.put("id", idContenedor.getId());
            JSONObject respuesta = Tools.enviarPeticionGET(Globales.dameObtenerContenedor(), parametros);

            if (!respuesta.getBoolean("estado")) {
                mensajeError = respuesta.getString("mensaje");
                return false;
            }

            JSONObject objetoRespuesta = respuesta.getJSONObject("respuesta");
            JSONArray objeto = objetoRespuesta.getJSONArray("partidas");
            Contenedor con = Globales.cargarFracciones(Fraccion.dameListFraccionJson(objeto), idContenedor.getId());
            con.guardarFracciones(contexto);

        } catch (Exception ex) {
            ex.printStackTrace();
            if (mensajeError.equals(""))
                mensajeError = "No se ha podido establecer una conexión con el servidor de Logiety";
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        if (!result) {
            Snackbar.make(vista, mensajeError, Snackbar.LENGTH_SHORT).show();

        } else {
            idContenedor.setEstado("Aceptado");
            fragment.irPerfilContenedor(idContenedor);
        }
    }
}

