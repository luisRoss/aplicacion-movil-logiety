package com.logiety.app.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.logiety.app.bean.Contenedor;
import com.logiety.app.bean.Fraccion;
import com.logiety.app.bean.Imagen;
import com.logiety.app.dao.ImagenDao;
import com.logiety.app.fragment.MostrarContenedorFragment;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.MultipartUtility;
import com.logiety.app.utils.Tools;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EnviarContenedorAsyncTask extends AsyncTask<String, Integer, Boolean> {
    private Context contexto;
    private View view;
    private ProgressDialog dialogoProgreso;

    private String mensajeError;
    private int numeroTotalImagenesAEnviar = 0;

    private ImagenDao daoI;

    private Contenedor contenedor;
    private ArrayList<Imagen> imagenesContenedorApertura;
    private ArrayList<Imagen> imagenesContenedorCierre;

    private ArrayList<Fraccion> fracciones;
    private HashMap<String, ArrayList<Imagen>> hashImagenesFracciones;

    private MostrarContenedorFragment frag;

    public EnviarContenedorAsyncTask(Context contexto, View view, Contenedor contenedor, MostrarContenedorFragment frag) {
        this.contenedor = contenedor;
        this.contexto = contexto;
        this.view = view;
        this.mensajeError = "";
        this.hashImagenesFracciones = new HashMap<>();
        this.daoI = new ImagenDao(contexto);
        this.frag = frag;
    }

    @Override
    protected void onPreExecute() {

        //Se obtiene el contenedor
        contenedor.obtenerApertura(contexto);
        contenedor.obtenerCierre(contexto);
        contenedor.obtenerFraccciones(contexto);

        this.imagenesContenedorApertura = daoI.obtenerTodos(contenedor.getIdApertura());
        numeroTotalImagenesAEnviar += this.imagenesContenedorApertura.size();

        this.imagenesContenedorCierre = daoI.obtenerTodos(contenedor.getIdCierre());
        numeroTotalImagenesAEnviar += this.imagenesContenedorCierre.size();

        if (!contenedor.listoEnvio() || imagenesContenedorApertura.size() == 0 ||
                imagenesContenedorCierre.size() == 0) {
            mensajeError = "No se puede enviar porque no se han cargado las imagenes de la apertura o el cierre";
            return;
        }

        System.out.println("Se envía el contenedor");
        System.out.println(contenedor);

        // Se obtienen la información de las fracciones
        fracciones = contenedor.dameList();
        for (Fraccion fraccion : fracciones) {
            fraccion.obtener(contexto);
            ArrayList<Imagen> imagenesFraccion = daoI.obtenerTodos(fraccion.getId());
            numeroTotalImagenesAEnviar += imagenesFraccion.size();

            if (!fraccion.listoEnvio() || imagenesFraccion.size() == 0) {
                System.out.println("imagenes " + imagenesFraccion.size());
                System.out.println("listo envio " + fraccion.listoEnvio());
                System.out.println(fraccion);
                mensajeError = "No se puede enviar porque no se han cargado las imagenes de todas las partidas";
                return;
            }

            this.hashImagenesFracciones.put(fraccion.getId(), imagenesFraccion);
        }


        System.out.println("Se envían las fracciones");
        for (Fraccion fraccion : fracciones)
            System.out.println(fraccion);

        dialogoProgreso = new ProgressDialog(contexto);
        dialogoProgreso.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogoProgreso.setTitle("Enviando el contenedor ");
        dialogoProgreso.setCancelable(false);
        dialogoProgreso.setMax(100);

        dialogoProgreso.show();
    }

    protected Boolean doInBackground(String... params) {
        if (!mensajeError.isEmpty()) return false;

        try {

            //Se envía la cabecera
            System.out.println("Se envía la información");
            establecerPorcentaje(0);
            enviarInicio();

            System.out.println("Se ha creado el contenedor en el servidor");
            establecerPorcentaje(10);

            double avancePorcentaje = 1.0 / numeroTotalImagenesAEnviar * 80.0;

            //Se envía la apertura
            int contador = 0;
            for (Imagen imagen : imagenesContenedorApertura) {
                enviarImagen(contenedor.getIdApertura(), imagen, contador);
                contador++;

                establecerPorcentaje(10 + (contador * avancePorcentaje));
            }
            imagenesContenedorApertura.clear();
            imagenesContenedorApertura = null;

            // Se envían las imágenes del cierre
            int contadorActual = 0;
            for (Imagen imagen : imagenesContenedorCierre) {
                enviarImagen(contenedor.getIdCierre(), imagen, contadorActual);
                contador++;
                contadorActual++;

                establecerPorcentaje(10 + (contador * avancePorcentaje));
            }
            imagenesContenedorCierre.clear();
            imagenesContenedorApertura = null;

            //Se envían las imagenes de la fracción
            for (Fraccion fraccion : fracciones) {
                contadorActual = 0;

                for (Imagen imagen : hashImagenesFracciones.get(fraccion.getId())) {
                    enviarImagen(fraccion.getId(), imagen, contadorActual);
                    contador++;
                    contadorActual++;

                    establecerPorcentaje(10 + (contador * avancePorcentaje));
                }
            }
            hashImagenesFracciones.clear();
            hashImagenesFracciones = null;

            System.out.println("Se cierra el contenedor");
            establecerPorcentaje(90);
            enviarCerrarContenedor(contenedor.getId());
            establecerPorcentaje(100);


        } catch (Exception ex) {
            ex.printStackTrace();
            contenedor.setAvance("0");
            mensajeError = "Sucedió un error inesperado al enviar el contenedor";
            return false;
        }

        return true;
    }

    private void establecerPorcentaje(double porcentaje) {
        dialogoProgreso.setProgress((int) porcentaje);
    }

    private void enviarInicio() throws Exception {

        MultipartUtility multipart = new MultipartUtility(Globales.dameRutaIngresarContenedoresInicio(), "UTF-8");

        multipart.addFormField("key", Tools.obtenerDatos(contexto, "cadenaConexion"));
        multipart.addFormField("id", contenedor.getId());
        multipart.addFormField("numero_partidas", String.valueOf(contenedor.getFracciones().size()));

        multipart.addFormField("nota_apertura", contenedor.getNotasApertura());
        multipart.addFormField("tiempo_inicio_apertura", contenedor.getTiempoInicioApertura());
        multipart.addFormField("tiempo_fin_apertura", contenedor.getTiempoFinApertura());
        multipart.addFormField("numero_imagenes_apertura", String.valueOf(imagenesContenedorApertura.size()));

        multipart.addFormField("nota_cierre", contenedor.getNotasCierre());
        multipart.addFormField("tiempo_inicio_cierre", contenedor.getTiempoInicioCierre());
        multipart.addFormField("tiempo_fin_cierre", contenedor.getTiempoFinCierre());
        multipart.addFormField("numero_imagenes_cierre", String.valueOf(imagenesContenedorCierre.size()));

        //Se procede a ingresar las fracciones
        for (Fraccion fraccionApoyo : fracciones) {
            String inicio = "partida-" + fraccionApoyo.getId();
            multipart.addFormField(inicio + "-notas", fraccionApoyo.getNotas());
            multipart.addFormField(inicio + "-fechaApertura", fraccionApoyo.getFechaApertura());
            multipart.addFormField(inicio + "-fechaCierre", fraccionApoyo.getFechaCierre());
            int numeroImagenes = hashImagenesFracciones.get(fraccionApoyo.getId()).size();
            multipart.addFormField(inicio + "-numeroimagenes", String.valueOf(numeroImagenes));
        }

        List<String> response = multipart.finish();

        System.out.println("Respuesta del servidor:");
        JSONObject objeto = new JSONObject();
        for (String line : response) {
            System.out.println(line);
            objeto = new JSONObject(line);
        }
        System.out.println("");

        if (!objeto.getBoolean("estado"))
            throw new Exception(objeto.getString("mensaje"));
    }

    private void enviarImagen(String id, Imagen imagenApoyo, int numeroImagen) throws Exception {
        MultipartUtility multipart = new MultipartUtility(Globales.dameRutaIngresarContenedoresImagenes(), "UTF-8");
        multipart.addFormField("key", Tools.obtenerDatos(contexto, "cadenaConexion"));
        multipart.addFormField("id", id);
        multipart.addFormField("numero_imagen", String.valueOf(numeroImagen));

        Uri myUri = Uri.parse(imagenApoyo.getRuta());

        multipart.addFormField("orientacion", Tools.decodeBitmapOrientation(contexto, myUri));

        InputStream input = Tools.decodeUriSend(contexto, myUri);
        multipart.addFilePart("imagen", "archivo.png", input);
        input.close();

        List<String> response = multipart.finish();

        System.out.println("Respuesta del servidor:");
        JSONObject objeto = new JSONObject();
        for (String line : response) {
            objeto = new JSONObject(line);
            System.out.println(objeto);
        }

        if (objeto.getBoolean("estado")) {
            JSONObject respuesta = objeto.getJSONObject("respuesta");

        } else
            throw new Exception("No se recibió la cadena correcta");
    }

    private void enviarCerrarContenedor(String idContenedor) throws Exception {
        MultipartUtility multipart = new MultipartUtility(Globales.dameRutaIngresarContenedoresCierre(), "UTF-8");
        multipart.addFormField("key", Tools.obtenerDatos(contexto, "cadenaConexion"));
        multipart.addFormField("id", idContenedor);

        List<String> response = multipart.finish();

        System.out.println("Respuesta del servidor:");
        JSONObject objeto = new JSONObject();
        for (String line : response) {
            objeto = new JSONObject(line);
            System.out.println(line);
        }

        if (objeto.getBoolean("estado")) {
            JSONObject respuesta = objeto.getJSONObject("respuesta");

        } else
            throw new Exception("No se recibió la cadena correcta");
    }


    @Override
    protected void onPostExecute(Boolean result) {
        if (dialogoProgreso != null)
            dialogoProgreso.dismiss();

        if (!result) {
            Snackbar.make(view, mensajeError, Snackbar.LENGTH_LONG).show();

        } else {
            Toast.makeText(contexto, "El contenedor se subió correctamente", Toast.LENGTH_LONG).show();
            contenedor.setEstado("Enviado");
            frag.actualizarLista();
        }
    }

}
