package com.logiety.app.asynctask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.logiety.app.bean.Contenedor;
import com.logiety.app.bean.Perfil;
import com.logiety.app.fragment.MostrarContenedorFragment;
import com.logiety.app.fragment.PerfilFragment;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.Tools;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class ObtenerDatosPerfilAsyncTask extends AsyncTask<String, Integer, Boolean> {
    private Context contexto;
    private String cadenaConexion;
    private View vista;
    private MostrarContenedorFragment contenedorGrid;
    private PerfilFragment perfilF;
    private Perfil perfil;
    private ArrayList<Contenedor> contenedores;

    private ProgressDialog dialogoProgreso;

    private String mensajeError;
    private String cadenaContenedores;
    private JSONObject respuesta;


    public ObtenerDatosPerfilAsyncTask(Context contexto, View vista, MostrarContenedorFragment grid, String cadenaConexion, PerfilFragment perfilF) {
        this.contexto = contexto;
        this.cadenaConexion = cadenaConexion;
        this.vista = vista;
        this.contenedorGrid = grid;
        this.perfilF = perfilF;
    }

    @Override
    protected void onPreExecute() {
        dialogoProgreso = new ProgressDialog(contexto);
        dialogoProgreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialogoProgreso.setTitle("Solicitando datos del contenedor");
        dialogoProgreso.setCancelable(false);
        dialogoProgreso.show();
    }

    protected Boolean doInBackground(String... params) {

        try {
            HashMap<String, String> parametros = new HashMap<>();
            parametros.put("cadena", cadenaConexion);
            JSONObject respuesta = Tools.enviarPeticionGET(Globales.dameSolicitarDatosPerfil(), parametros);

            if (!respuesta.getBoolean("estado")) {
                mensajeError = "Sucedió un error en el servidor al querer obtener la información";
                return false;
            }

            JSONObject objeto = respuesta.getJSONObject("respuesta");

            cadenaContenedores = objeto.getString("contenedores");
            contenedores = Contenedor.dameListadoContenedor("{'contenedores':" + cadenaContenedores + "}");

            JSONObject objetoPerfil = objeto.getJSONObject("perfil");
            perfil = new Perfil();
            perfil.cargarDeJson(objetoPerfil);

        } catch (Exception ex) {
            mensajeError = "No se ha podido establecer un contacto con el servidor de Logiety";
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        dialogoProgreso.dismiss();
        dialogoProgreso = null;

        if (!result) {
            Snackbar.make(vista, mensajeError, Snackbar.LENGTH_LONG).show();

        } else {
            Snackbar.make(vista, "Los contenedores se han actualizado correctamente", Snackbar.LENGTH_LONG).show();
            Globales.cargarInformacion(contenedores);
            Globales.setPerfil(perfil);

            contenedorGrid.actualizarLista();
            perfilF.actualizar();
        }
    }
}

