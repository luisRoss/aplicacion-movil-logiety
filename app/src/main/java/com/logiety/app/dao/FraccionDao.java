package com.logiety.app.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.logiety.app.bean.Fraccion;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.SQLiteHelper;

import java.util.ArrayList;

public class FraccionDao {

    private static String nombreTabla = "Fraccion";
    private static String primaria = "id";
    private static String[] campos = new String[]{"id", "notas", "fecha_apertura", "fecha_cierre", "id_usuario"};

    private Context contexto;

    public FraccionDao(Context contexto) {
        this.contexto = contexto;
    }

    public Fraccion obtener(String id) {
        SQLiteHelper FraccionDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = FraccionDatos.getWritableDatabase();
        Fraccion Fraccion = new Fraccion();

        Cursor cursorApoyo = baseDatos.query(nombreTabla, campos, primaria + "='" + id + "'",
                null, null, null, null);

        if (cursorApoyo.moveToFirst())
            do Fraccion = dameFraccionDeCursor(cursorApoyo);
            while (cursorApoyo.moveToNext());

        cursorApoyo.close();
        baseDatos.close();
        FraccionDatos.close();
        return Fraccion;
    }

    public ArrayList<Fraccion> obtenerTodos(String idContenedor) {
        ArrayList<Fraccion> contenedores = new ArrayList<>();
        SQLiteHelper contenedorDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = contenedorDatos.getWritableDatabase();

        Cursor cursorApoyo = baseDatos.query(nombreTabla, campos, "id_usuario='" + idContenedor + "'"
                , null, null, null, null);

        if (cursorApoyo.moveToFirst())
            do contenedores.add(dameFraccionDeCursor(cursorApoyo));
            while (cursorApoyo.moveToNext());

        cursorApoyo.close();
        baseDatos.close();
        contenedorDatos.close();
        return contenedores;
    }

    public String guardar(Fraccion Fraccion) {
        SQLiteHelper FraccionDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = FraccionDatos.getWritableDatabase();

        baseDatos.insert(nombreTabla, null, obtenerContent(Fraccion));

        String[] campos = new String[]{primaria};
        Cursor cursor = baseDatos.query(nombreTabla, campos, null, null, null, null, null);
        cursor.moveToLast();
        String id_final = cursor.getString(0);

        Fraccion.setId(id_final);

        cursor.close();
        baseDatos.close();
        FraccionDatos.close();

        return id_final;
    }

    public void actualizar(Fraccion fraccion) {
        SQLiteHelper FraccionDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = FraccionDatos.getWritableDatabase();

        baseDatos.update(nombreTabla, obtenerContent(fraccion), primaria + "='" + fraccion.getId() + "'", null);

        baseDatos.close();
        FraccionDatos.close();
    }

    public void eliminar(String id) {
        SQLiteHelper FraccionDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = FraccionDatos.getWritableDatabase();

        baseDatos.delete(nombreTabla, primaria + "='" + id + "'", null);

        baseDatos.close();
        FraccionDatos.close();
    }

    private static ContentValues obtenerContent(Fraccion fraccion) {
        ContentValues nuevoRegistro = new ContentValues();

        nuevoRegistro.put("id", fraccion.getId());
        nuevoRegistro.put("notas", fraccion.getNotas());
        nuevoRegistro.put("fecha_apertura", fraccion.getFechaApertura());
        nuevoRegistro.put("fecha_cierre", fraccion.getFechaCierre());
        nuevoRegistro.put("id_usuario", fraccion.getId_usuario());

        return nuevoRegistro;
    }


    private static Fraccion dameFraccionDeCursor(Cursor cursorApoyo) {
        Fraccion fraccion = new Fraccion();

        fraccion.setId(cursorApoyo.getString(0));
        fraccion.setNotas(cursorApoyo.getString(1));
        fraccion.setFechaApertura(cursorApoyo.getString(2));
        fraccion.setFechaCierre(cursorApoyo.getString(3));
        fraccion.setId_usuario(cursorApoyo.getString(4));

        return fraccion;

    }
}
