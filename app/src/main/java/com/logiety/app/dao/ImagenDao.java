package com.logiety.app.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.logiety.app.bean.Imagen;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.SQLiteHelper;

import java.util.ArrayList;

public class ImagenDao {

    private static String nombreTabla = "Imagen";
    private static String primaria = "id";
    private static String[] campos = new String[]{"id", "id_fraccion", "ruta"};

    private Context contexto;

    public ImagenDao(Context contexto) {
        this.contexto = contexto;
    }

    public Imagen obtener(int id) {
        SQLiteHelper ImagenDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = ImagenDatos.getWritableDatabase();
        Imagen Imagen = new Imagen();

        Cursor cursorApoyo = baseDatos.query(nombreTabla, campos, primaria + "='" + id + "'", null, null, null, null);

        if (cursorApoyo.moveToFirst())
            do Imagen = obtenerImagen(cursorApoyo);
            while (cursorApoyo.moveToNext());

        cursorApoyo.close();
        baseDatos.close();
        ImagenDatos.close();
        return Imagen;
    }

    public void guardar(Imagen Imagen) {
        SQLiteHelper ImagenDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = ImagenDatos.getWritableDatabase();

        long id = baseDatos.insert(nombreTabla, null, obtenerContent(Imagen));

        baseDatos.close();
        ImagenDatos.close();
    }

    public ArrayList<Imagen> obtenerTodos(String numeroFraccion) {
        ArrayList<Imagen> Imagenes = new ArrayList<>();
        SQLiteHelper ImagenDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = ImagenDatos.getWritableDatabase();

        Cursor cursorApoyo = baseDatos.query(nombreTabla, campos, "id_fraccion='" + numeroFraccion + "'"
                , null, null, null, null);

        if (cursorApoyo.moveToFirst())
            do Imagenes.add(obtenerImagen(cursorApoyo));
            while (cursorApoyo.moveToNext());

        cursorApoyo.close();
        baseDatos.close();
        ImagenDatos.close();
        return Imagenes;
    }

    public void actualizar(Imagen Imagen) {
        SQLiteHelper ImagenDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = ImagenDatos.getWritableDatabase();

        baseDatos.update(nombreTabla, obtenerContent(Imagen), primaria +
                "=" + Imagen.getIdFraccion(), null);

        baseDatos.close();
        ImagenDatos.close();
    }

    public void eliminar(int id) {
        SQLiteHelper ImagenDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = ImagenDatos.getWritableDatabase();

        baseDatos.delete(nombreTabla, primaria + "=" + id, null);

        baseDatos.close();
        ImagenDatos.close();
    }

    private static ContentValues obtenerContent(Imagen imagen) {
        ContentValues nuevoRegistro = new ContentValues();

        nuevoRegistro.put("id_fraccion", imagen.getIdFraccion());
        nuevoRegistro.put("ruta", imagen.getRuta());

        return nuevoRegistro;
    }

    private static Imagen obtenerImagen(Cursor cursorApoyo) {
        Imagen imagen = new Imagen();

        imagen.setId(cursorApoyo.getInt(0));
        imagen.setIdFraccion(cursorApoyo.getString(1));
        imagen.setRuta(cursorApoyo.getString(2));

        return imagen;
    }
}
