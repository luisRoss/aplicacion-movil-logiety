package com.logiety.app.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.logiety.app.bean.Contenedor;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.SQLiteHelper;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ContenedorDao {

    private static String nombreTabla = "Contenedor";
    private static String primaria = "id";
    private static String[] campos =
            new String[]{"id", "fecha_aceptacion", "avance", "id_usuario",
                    "id_apertura", "nota_apertura", "tiempo_inicio_apertura", "tiempo_fin_apertura",
                    "id_cierre", "nota_cierre", "tiempo_inicio_cierre", "tiempo_fin_cierre"};

    private Context contexto;

    public ContenedorDao(Context contexto) {
        this.contexto = contexto;
    }

    public ArrayList<Contenedor> obtenerTodos(String numeroFraccion) {
        ArrayList<Contenedor> contenedores = new ArrayList<>();
        SQLiteHelper contenedorDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = contenedorDatos.getWritableDatabase();

        Cursor cursorApoyo = baseDatos.query(nombreTabla, campos, "id_usuario='" + numeroFraccion + "'"
                , null, null, null, null);

        if (cursorApoyo.moveToFirst())
            do contenedores.add(dameContenedorDeCursor(cursorApoyo));
            while (cursorApoyo.moveToNext());

        cursorApoyo.close();
        baseDatos.close();
        contenedorDatos.close();
        return contenedores;
    }

    public Contenedor obtener(String id) {
        SQLiteHelper contenedorDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = contenedorDatos.getWritableDatabase();
        Contenedor contenedor = new Contenedor();

        Cursor cursorApoyo = baseDatos.query(nombreTabla, campos, primaria + "='" + id + "'", null, null, null, null);

        if (cursorApoyo.moveToFirst())
            do contenedor = dameContenedorDeCursor(cursorApoyo);
            while (cursorApoyo.moveToNext());

        cursorApoyo.close();
        baseDatos.close();
        contenedorDatos.close();
        return contenedor;
    }

    public void guardar(Contenedor contenedor) {
        SQLiteHelper contenedorDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = contenedorDatos.getWritableDatabase();

        baseDatos.insert(nombreTabla, null, obtenerContent(contenedor));

        baseDatos.close();
        contenedorDatos.close();
    }

    public void actualizar(Contenedor contenedor) {
        SQLiteHelper contenedorDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = contenedorDatos.getWritableDatabase();

        baseDatos.update(nombreTabla, obtenerContent(contenedor), primaria + "='" + contenedor.getId()+ "'", null);

        baseDatos.close();
        contenedorDatos.close();
    }

    public void eliminar(String id) {
        SQLiteHelper contenedorDatos = new SQLiteHelper(contexto, Globales.DATABASE_NAME, null, Globales.VERSION_DATABASE);
        SQLiteDatabase baseDatos = contenedorDatos.getWritableDatabase();

        baseDatos.delete(nombreTabla, primaria + "=" + id, null);

        baseDatos.close();
        contenedorDatos.close();
    }

    private ContentValues obtenerContent(Contenedor contenedor) {
        ContentValues nuevoRegistro = new ContentValues();

        nuevoRegistro.put("id", contenedor.getId());
        nuevoRegistro.put("fecha_aceptacion", contenedor.getFechaAceptacion());
        nuevoRegistro.put("id_usuario", contenedor.getIdUsuario());
        nuevoRegistro.put("avance", contenedor.getAvance());

        nuevoRegistro.put("id_apertura", contenedor.getIdApertura());
        nuevoRegistro.put("nota_apertura", contenedor.getNotasApertura());
        nuevoRegistro.put("tiempo_inicio_apertura", contenedor.getTiempoInicioApertura());
        nuevoRegistro.put("tiempo_fin_apertura", contenedor.getTiempoFinApertura());

        nuevoRegistro.put("id_cierre", contenedor.getIdCierre());
        nuevoRegistro.put("nota_cierre", contenedor.getNotasCierre());
        nuevoRegistro.put("tiempo_inicio_cierre", contenedor.getTiempoInicioCierre());
        nuevoRegistro.put("tiempo_fin_cierre", contenedor.getTiempoFinCierre());

        return nuevoRegistro;
    }

    private Contenedor dameContenedorDeCursor(Cursor cursorApoyo) {
        Contenedor contenedor = new Contenedor();

        contenedor.setId(cursorApoyo.getString(0));
        contenedor.setFechaAceptacion(cursorApoyo.getString(1));
        contenedor.setIdUsuario(cursorApoyo.getString(2));
        contenedor.setAvance(cursorApoyo.getString(3));

        contenedor.setIdApertura(cursorApoyo.getString(4));
        contenedor.setNotasApertura(cursorApoyo.getString(5));
        contenedor.setTiempoInicioApertura(cursorApoyo.getString(6));
        contenedor.setTiempoFinApertura(cursorApoyo.getString(7));

        contenedor.setIdCierre(cursorApoyo.getString(8));
        contenedor.setNotasCierre(cursorApoyo.getString(9));
        contenedor.setTiempoInicioCierre(cursorApoyo.getString(10));
        contenedor.setTiempoFinCierre(cursorApoyo.getString(11));

        return contenedor;
    }
}
