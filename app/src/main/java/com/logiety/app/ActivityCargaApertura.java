package com.logiety.app;

import android.app.Activity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.logiety.app.adapter.ImagenesGridAdapter;
import com.logiety.app.bean.Contenedor;
import com.logiety.app.bean.Imagen;
import com.logiety.app.dao.ImagenDao;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.Tools;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityCargaApertura extends Activity {
    private RecyclerView recyclerView;
    private ImagenesGridAdapter mGridAdapter;
    private List<Imagen> imagenes = new ArrayList<>();
    private boolean modoEliminarFoto;
    private Uri photoURI;
    private Contenedor contenedor;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captura_apertura);

        contenedor = Globales.dameContenedor(getIntent().getStringExtra("id"));
        contenedor.obtenerApertura(getApplicationContext());

        modoEliminarFoto = false;

        //No existe previamente un contenedor
        if (contenedor.esInicioApertura()) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date now = new Date();
            contenedor.setTiempoInicioApertura(dateFormat.format(now));
            contenedor.guardarApertura(getApplicationContext());
        }

        cargarDatosModoEdicion();

        ImagenDao dao = new ImagenDao(getApplicationContext());
        imagenes = dao.obtenerTodos(contenedor.getIdApertura());

        recyclerView = (RecyclerView) findViewById(R.id.galleryRecycler);

        GridLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), Tools.getGridExplorerCount(this));

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setPadding(2, 2, 2, 2);

        Button boton = (Button) findViewById(R.id.perfil_apertura_agregar_imagen);
        registerForContextMenu(boton);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modoEliminarFoto = false;
                openContextMenu(view);
            }
        });

        //set data and list adapter
        mGridAdapter = new ImagenesGridAdapter(getApplicationContext(), imagenes);
        recyclerView.setAdapter(mGridAdapter);

        registerForContextMenu(recyclerView);

        mGridAdapter.setOnItemClickListener(new ImagenesGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Imagen obj, int position) {
                modoEliminarFoto = true;
                openContextMenu(view);
            }
        });

        mGridAdapter.setmOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                try {
                    modoEliminarFoto = true;
                    openContextMenu(view);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return false;
            }
        });
    }

    public void cargarDatosModoEdicion() {

        TextView texto = (TextView) findViewById(R.id.perfil_apertura_notas);
        texto.setText(contenedor.getNotasApertura());

        TextView textoGuardar = (TextView) findViewById(R.id.perfil_partida_guardar);
        textoGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImagenDao imagenDao = new ImagenDao(getApplicationContext());
                ArrayList<Imagen> imagenesPrevias = imagenDao.obtenerTodos(contenedor.getIdApertura());
                for (Imagen imagen : imagenesPrevias)
                    imagenDao.eliminar(imagen.getId());

                imagenDao = new ImagenDao(getApplicationContext());
                for (Imagen imagenApoyo : imagenes) {
                    imagenApoyo.setIdFraccion(contenedor.getIdApertura());
                    imagenDao.guardar(imagenApoyo);
                }

                TextView texto = (TextView) findViewById(R.id.perfil_apertura_notas);
                contenedor.setNotasApertura(texto.getText().toString());
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                contenedor.setTiempoFinApertura(dateFormat.format(new Date()));
                contenedor.guardarApertura(getApplicationContext());

                Intent i = new Intent(getApplicationContext(), ActivityMostrarPerfilContenedor.class);
                i.putExtra("id", contenedor.getId());
                i.putExtra("guardado", true);
                startActivity(i);
            }
        });
    }

    public void cargarImagenes() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/*");
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

        startActivityForResult(Intent.createChooser(i, "Selecciona múltiples imágenes"), 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {

                    ClipData clip = data.getClipData();

                    if (clip == null) {
                        Imagen imagen = new Imagen();

                        File storage = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                        Uri uri = Tools.guardarArchivo(data.getData(), getApplicationContext(), storage);
                        imagen.setRuta(uri.toString());
                        imagenes.add(imagen);
                        mGridAdapter.notifyDataSetChanged();

                    } else {
                        for (int contador = 0; contador < clip.getItemCount(); contador++) {
                            ClipData.Item it = clip.getItemAt(contador);
                            Imagen imagen = new Imagen();

                            File storage = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                            Uri uri = Tools.guardarArchivo(it.getUri(), getApplicationContext(), storage);
                            imagen.setRuta(uri.toString());
                            imagenes.add(imagen);
                            mGridAdapter.notifyDataSetChanged();
                        }
                    }
                }
                break;
            case 2:

                if (resultCode == Activity.RESULT_OK && photoURI != null) {
                    Imagen imagen = new Imagen();
                    imagen.setRuta(photoURI.toString());
                    imagenes.add(imagen);
                }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (modoEliminarFoto) {
            menu.setHeaderTitle("Acciones");
            menu.add(0, v.getId(), 0, "Eliminar");
        } else {
            menu.setHeaderTitle("Cargar imagen");
            menu.add(0, v.getId(), 0, "Cámara");
            menu.add(0, v.getId(), 0, "Archivo");
        }

    }


    public boolean onContextItemSelected(MenuItem item) {
        String accion = item.getTitle().toString();

        if (modoEliminarFoto) {
            switch (accion) {

                case "Eliminar":
                    mGridAdapter.eliminarSeleccionado();
                    break;

                default:
                    System.out.println("mostrar default");
            }

        } else {
            switch (accion) {

                case "Cámara":
                    tomarImagen();
                    break;

                case "Archivo":
                    cargarImagenes();
                    break;

                default:
                    System.out.println("mostrar default");
            }
        }

        return super.onContextItemSelected(item);
    }

    private void tomarImagen() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {

                File storage = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                photoFile = Tools.createImageFile(storage);

            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 2);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setTitle("Regresar")
                    .setMessage("No has guardado los cambios, ¿Deseas salir?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Intent i = new Intent(getApplicationContext(), ActivityMostrarPerfilContenedor.class);
                            i.putExtra("id", contenedor.getId());
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();

            return true;
        }
        return false;
    }
}
