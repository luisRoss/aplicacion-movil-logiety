package com.logiety.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.logiety.app.adapter.PartidaListAdapter;
import com.logiety.app.bean.Contenedor;
import com.logiety.app.bean.Fraccion;
import com.logiety.app.utils.Globales;

import java.util.Collections;
import java.util.List;

public class ActivityMostrarListadoPartida extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PartidaListAdapter mAdapter;
    private Contenedor contenedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenedor_listado_partidas);

        contenedor = Globales.dameContenedor(getIntent().getStringExtra("id"));

        boolean guardar = getIntent().getBooleanExtra("guardado", false);
        if (guardar) {
            Toast toast1 = Toast.makeText(getApplicationContext(), "Partida guardada correctamente", Toast.LENGTH_SHORT);
            toast1.show();
        }

        //Se cargan los datos
        TextView text = (TextView) findViewById(R.id.perfil_contenedor_numero_contendor);
        text.setText(contenedor.getNumero());

        text = (TextView) findViewById(R.id.perfil_contenedor_numero_sello);
        text.setText(contenedor.getNumeroReferencia());

        text = (TextView) findViewById(R.id.perfil_contenedor_cliente);
        text.setText("Cliente: " + contenedor.getCliente());

        text = (TextView) findViewById(R.id.perfil_contenedor_fecha);
        text.setText("Fecha de creación: " + contenedor.getFechaCreacion());
        text = (TextView) findViewById(R.id.perfil_contenedor_estado);
        text.setText(contenedor.isEnviado() ? "Enviado" : contenedor.getEstado());

        if (contenedor.isEnviado()) {

        } else if (contenedor.getEstado().equals("Aceptado"))
            text.setBackgroundColor(text.getContext().getResources().getColor(R.color.aceptado_contenedor));
        else
            text.setBackgroundColor(text.getContext().getResources().getColor(R.color.pendiente_contenedor));

        ImageView imagen = (ImageView) findViewById(R.id.perfil_contenedor_icono);

        int recurso = 0;
        if (contenedor.isEnviado())
            recurso = R.drawable.icono_enviado;
        else if (contenedor.getEstado().equals("Aceptado"))
            recurso = R.drawable.icono_aceptado;
        else
            recurso = R.drawable.icono_pendiente;

        imagen.setImageResource(recurso);


        List<Fraccion> items_following = contenedor.dameList();
        for (Fraccion fraccion : items_following)
            fraccion.obtener(this);

        Collections.sort(items_following);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        registerForContextMenu(recyclerView);

        mAdapter = new PartidaListAdapter(this, items_following);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new PartidaListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Fraccion obj, int position) {
                openContextMenu(v);

                Intent i = new Intent(getApplicationContext(), ActivityCargarPartida.class);
                i.putExtra("idFraccion", obj.getId());
                i.putExtra("id", contenedor.getId());
                startActivity(i);
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(getApplicationContext(), ActivityMostrarPerfilContenedor.class);
            i.putExtra("id", contenedor.getId());
            startActivity(i);

            return true;
        }
        return false;
    }
}
