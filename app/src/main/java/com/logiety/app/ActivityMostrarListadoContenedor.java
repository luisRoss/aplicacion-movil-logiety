package com.logiety.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.logiety.app.adapter.PageFragmentAdapter;
import com.logiety.app.asynctask.EnviarContenedorAsyncTask;
import com.logiety.app.asynctask.LiberarContenedorAsyncTask;
import com.logiety.app.asynctask.ObtenerDatosPerfilAsyncTask;
import com.logiety.app.bean.Contenedor;
import com.logiety.app.fragment.MostrarContenedorFragment;
import com.logiety.app.fragment.PerfilFragment;
import com.logiety.app.utils.Tools;


public class ActivityMostrarListadoContenedor extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private PageFragmentAdapter adapter;
    private Toolbar toolbar;
    private MostrarContenedorFragment f_contenedores;
    private PerfilFragment f_perfil;
    private ActionBar actionbar;

    private String cadenaRespuestaPeticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_contenedor);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setTitle("");

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();
        setupTabClick();

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
        String cadenaConexion = Tools.obtenerDatos(getApplicationContext(), "cadenaConexion");
        new ObtenerDatosPerfilAsyncTask(ActivityMostrarListadoContenedor.this, viewPager,
                f_contenedores, cadenaConexion, f_perfil).execute();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new PageFragmentAdapter(getSupportFragmentManager());
        cadenaRespuestaPeticion = Tools.obtenerDatos(getApplicationContext(), "perfil");

        if (f_contenedores == null) {
            f_contenedores = new MostrarContenedorFragment();
        }

        if (f_perfil == null) {
            f_perfil = new PerfilFragment();
        }

        adapter.addFragment(f_contenedores, getString(R.string.tab_contenedores));
        adapter.addFragment(f_perfil, getString(R.string.tab_perfil));
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_container);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_user_white);
    }

    private void setupTabClick() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    public void cerrarSesion(View v) {
        Tools.guardarDatos(getApplicationContext(), "cadenaConexion", "");
        Tools.guardarDatos(getApplicationContext(), "perfil", "");

        Intent i = new Intent(getApplicationContext(), ActivityLogin.class);
        startActivity(i);
    }

    public void actualizar(View v) {
        String cadena = Tools.obtenerDatos(getApplicationContext(), "cadenaConexion");
        new ObtenerDatosPerfilAsyncTask(ActivityMostrarListadoContenedor.this, viewPager,
                f_contenedores, cadena, f_perfil).execute();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        RecyclerView lv = (RecyclerView) v;
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;

        Contenedor contenedor = f_contenedores.mAdapter.getIdContenedorSeleccionado();
        if (contenedor.isEnviado()) {
            menu.setHeaderTitle("Acciones");
            menu.add(0, v.getId(), 0, "Enviar");

        } else if (contenedor.getEstado().equals("Aceptado")) {
            menu.setHeaderTitle("Acciones");
            menu.add(0, v.getId(), 0, "Enviar");
            menu.add(0, v.getId(), 0, "Liberar");

        } else {

        }
    }


    public boolean onContextItemSelected(MenuItem item) {
        String accion = item.getTitle().toString();

        switch (accion) {
            case "Enviar":
                Contenedor contenedor = f_contenedores.mAdapter.getIdContenedorSeleccionado();
                new EnviarContenedorAsyncTask(ActivityMostrarListadoContenedor.this, viewPager,
                        contenedor, f_contenedores).execute();
                break;
            case "Liberar":
                contenedor = f_contenedores.mAdapter.getIdContenedorSeleccionado();
                String cadenaConexion = Tools.obtenerDatos(getApplicationContext(), "cadenaConexion");
                new LiberarContenedorAsyncTask(f_contenedores, viewPager, ActivityMostrarListadoContenedor.this,
                        cadenaConexion, contenedor).execute();
                break;

            default:
                System.out.println("mostrar default");
        }

        return super.onContextItemSelected(item);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return keyCode == KeyEvent.KEYCODE_BACK;
    }


}
