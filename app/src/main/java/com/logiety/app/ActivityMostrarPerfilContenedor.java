package com.logiety.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.logiety.app.bean.Contenedor;
import com.logiety.app.utils.Globales;

public class ActivityMostrarPerfilContenedor extends AppCompatActivity {

    private String id;
    private Contenedor contenedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenedor_perfil);

        contenedor = Globales.dameContenedor(getIntent().getStringExtra("id"));
        boolean guardar = getIntent().getBooleanExtra("guardado", false);
        if (guardar) {
            Toast toast1 = Toast.makeText(getApplicationContext(), "Partida guardada correctamente", Toast.LENGTH_SHORT);
            toast1.show();
        }


        //Se cargan los datos
        TextView text = (TextView) findViewById(R.id.perfil_contenedor_numero_contendor);
        text.setText(contenedor.getNumero());

        text = (TextView) findViewById(R.id.perfil_contenedor_numero_sello);
        text.setText(contenedor.getNumeroReferencia());

        text = (TextView) findViewById(R.id.perfil_contenedor_cliente);
        text.setText("Cliente: " + contenedor.getCliente());

        text = (TextView) findViewById(R.id.perfil_contenedor_fecha);
        text.setText("Fecha de creación: " + contenedor.getFechaCreacion());

        text = (TextView) findViewById(R.id.perfil_contenedor_estado);
        text.setText(contenedor.isEnviado() ? "Enviado" : contenedor.getEstado());

        if (contenedor.isEnviado()) {

        } else if (contenedor.getEstado().equals("Aceptado"))
            text.setBackgroundColor(text.getContext().getResources().getColor(R.color.aceptado_contenedor));
        else
            text.setBackgroundColor(text.getContext().getResources().getColor(R.color.pendiente_contenedor));

        ImageView imagen = (ImageView) findViewById(R.id.perfil_contenedor_icono);

        int recurso = 0;
        if (contenedor.isEnviado())
            recurso = R.drawable.icono_enviado;
        else if (contenedor.getEstado().equals("Aceptado"))
            recurso = R.drawable.icono_aceptado;
        else
            recurso = R.drawable.icono_pendiente;

        imagen.setImageResource(recurso);
    }

    public void actionClick(View v) {
        Intent i = new Intent(getApplicationContext(), ActivityCargarPartida.class);
        i.putExtra("idContenedor", id);
        i.putExtra("modo", "nuevo");
        startActivity(i);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(getApplicationContext(), ActivityMostrarListadoContenedor.class);
            startActivity(i);

            return true;
        }
        return false;
    }

    public void cargarApertura(View v) {
        Intent i = new Intent(getApplicationContext(), ActivityCargaApertura.class);
        i.putExtra("id", contenedor.getId());
        startActivity(i);
    }

    public void mostrarPartidas(View v) {
        Intent i = new Intent(getApplicationContext(), ActivityMostrarListadoPartida.class);
        i.putExtra("id", contenedor.getId());
        startActivity(i);
    }

    public void cargarCierre(View v) {
        Intent i = new Intent(getApplicationContext(), ActivityCargaCierre.class);
        i.putExtra("id", contenedor.getId());
        startActivity(i);
    }


}
