package com.logiety.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.logiety.app.utils.Tools;
import com.logiety.app.utils.Globales;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ActivityLogin extends AppCompatActivity {
    private EditText input_username, input_password;
    private Button btnSignUp;
    private View parent_view;
    private ProgressDialog dialogoProgreso;
    private boolean errorLogueo;
    private boolean errorConexion;
    private boolean errorServidor;
    private String cadenaConexion;
    private String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //En caso de que existan las cadenas de conexión y el perfil entonces se procede para al perfil
        String cadenaConexion = Tools.obtenerDatos(getApplicationContext(), "cadenaConexion");

        if (cadenaConexion != null && !cadenaConexion.equals("")) {
            System.out.println("Sesión previamente creada");
            Intent i = new Intent(getApplicationContext(), ActivityMostrarListadoContenedor.class);
            startActivity(i);

        } else {
            System.out.println("No hay sesión previa");

        }

        parent_view = findViewById(android.R.id.content);

        input_username = (EditText) findViewById(R.id.input_username);
        input_password = (EditText) findViewById(R.id.input_password);


        btnSignUp = (Button) findViewById(R.id.btn_signup);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        hideKeyboard(parent_view);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validarFormulario()) {
            Snackbar.make(parent_view, "El usuario o la contraseña no pueden estar vacios", Snackbar.LENGTH_SHORT).show();
            return;
        }

        new TareaAcceso(input_username.getText().toString(),
                input_password.getText().toString()).execute();
    }

    private boolean validarFormulario() {
        if (input_username.getText().toString().trim().isEmpty() || input_password.getText().toString().trim().isEmpty()) {
            requestFocus(input_username);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return false;
    }


    class TareaAcceso extends AsyncTask<String, Integer, Boolean> {
        private String nombreUsuario;
        private String contrasena;

        public TareaAcceso(String nombreUsuario, String contrasena) {
            this.nombreUsuario = nombreUsuario;
            this.contrasena = contrasena;
        }

        @Override
        protected void onPreExecute() {
            dialogoProgreso = new ProgressDialog(ActivityLogin.this);
            dialogoProgreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogoProgreso.setTitle("Conectando...");
            //dialogoProgreso.setIndeterminate(true);
            dialogoProgreso.show();
        }

        protected Boolean doInBackground(String... params) {
            errorLogueo = false;
            errorConexion = false;
            errorServidor = false;

            try {
                HashMap<String, String> parametros = new HashMap<>();
                parametros.put("usuario", nombreUsuario);
                parametros.put("contrasena", contrasena);

                JSONObject respuesta = Tools.enviarPeticionGET(Globales.dameRutaAcceso(), parametros);
                JSONObject objeto = respuesta.getJSONObject("respuesta");

                if (!objeto.getBoolean("estado")) {
                    errorLogueo = true;

                } else {

                    if (!respuesta.getBoolean("estado")) {
                        errorServidor = true;
                        return true;
                    }

                    cadenaConexion = objeto.getString("keyConexion");
                    System.out.println("cadena conexion");
                    System.out.println(cadenaConexion);
                    idUsuario = objeto.getString("id_usuario");
                }


            } catch (JSONException e) {
                e.printStackTrace();
                errorServidor = true;

            } catch (Exception ex) {
                ex.printStackTrace();
                errorConexion = true;

            }


            return true;
        }


        @Override
        protected void onPostExecute(Boolean result) {
            hideKeyboard();
            dialogoProgreso.dismiss();

            if (errorConexion) {
                Snackbar.make(parent_view, "No se ha podido establecer conexión con el servidor", Snackbar.LENGTH_SHORT).show();
                return;
            }

            if (errorLogueo) {
                Snackbar.make(parent_view, "El usuario o la contraseña son incorrectos", Snackbar.LENGTH_SHORT).show();
                return;
            }

            if (errorServidor) {
                Snackbar.make(parent_view, "No se ha podido establecer una conexión, volver a intentar", Snackbar.LENGTH_SHORT).show();
                return;
            }

            Tools.guardarDatos(getApplicationContext(), "cadenaConexion", cadenaConexion);
            Tools.guardarDatos(getApplicationContext(), "idUsuario", idUsuario);

            String cadenaConexion = Tools.obtenerDatos(getApplicationContext(), "cadenaConexion");
            System.out.println("cadena conexion después de guardar");
            System.out.println(cadenaConexion);

            Intent i = new Intent(getApplicationContext(), ActivityMostrarListadoContenedor.class);
            startActivity(i);
        }
    }
}
