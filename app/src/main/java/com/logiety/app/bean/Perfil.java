package com.logiety.app.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class Perfil {
    private String nombreCompleto;
    private String empresa;
    private String contenedoresEnviados;
    private String contenedoresPendientes;
    private String imagenesEnviadas;
    private String nombreUsuario;
    private String gafete;
    private String correoElectronico;
    private String ultimoContenedorEnviado;
    private String fechaCreacion;

    public Perfil() {
        nombreCompleto = "";
        empresa = "";
        contenedoresEnviados = "";
        contenedoresPendientes = "";
        imagenesEnviadas = "";
        nombreUsuario = "";
        gafete = "";
        correoElectronico = "";
        ultimoContenedorEnviado = "";
        fechaCreacion = "";
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getContenedoresEnviados() {
        return contenedoresEnviados;
    }

    public void setContenedoresEnviados(String contenedoresEnviados) {
        this.contenedoresEnviados = contenedoresEnviados;
    }

    public String getContenedoresPendientes() {
        return contenedoresPendientes;
    }

    public void setContenedoresPendientes(String contenedoresPendientes) {
        this.contenedoresPendientes = contenedoresPendientes;
    }

    public String getImagenesEnviadas() {
        return imagenesEnviadas;
    }

    public void setImagenesEnviadas(String imagenesEnviadas) {
        this.imagenesEnviadas = imagenesEnviadas;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getGafete() {
        return gafete;
    }

    public void setGafete(String gafete) {
        this.gafete = gafete;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getUltimoContenedorEnviado() {
        return ultimoContenedorEnviado;
    }

    public void setUltimoContenedorEnviado(String ultimoContenedorEnviado) {
        this.ultimoContenedorEnviado = ultimoContenedorEnviado;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }


    public void cargarDeJson(JSONObject objeto) throws JSONException {
        this.setNombreCompleto(objeto.getString("nombre_completo"));
        this.setEmpresa(objeto.getString("empresa"));
        this.setContenedoresEnviados(objeto.getString("contenedores_enviados"));
        this.setImagenesEnviadas(objeto.getString("imagenes_enviadas"));
        this.setNombreUsuario(objeto.getString("nombre_usuario"));
        this.setGafete(objeto.getString("gafete"));
        this.setCorreoElectronico(objeto.getString("correo_electronico"));
        this.setUltimoContenedorEnviado(objeto.getString("ultimo_contenedor_enviado"));
        this.setFechaCreacion(objeto.getString("fecha_creacion"));
    }
}
