package com.logiety.app.bean;

public class Imagen {
    private int id;
    private String idFraccion;
    private String ruta;

    public Imagen() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdFraccion() {
        return idFraccion;
    }

    public void setIdFraccion(String idFraccion) {
        this.idFraccion = idFraccion;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    @Override
    public String toString() {
        return "Imagen{" +
                "id=" + id +
                ", idFraccion='" + idFraccion + '\'' +
                ", ruta='" + ruta + '\'' +
                '}';
    }
}
