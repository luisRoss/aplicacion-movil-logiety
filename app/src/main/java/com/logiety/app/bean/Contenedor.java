package com.logiety.app.bean;

import android.content.Context;

import com.logiety.app.utils.Globales;
import com.logiety.app.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Contenedor implements Serializable, Comparator<Contenedor> {
    private String id;
    private String numero;
    private String numeroReferencia;
    private String fechaCreacion;
    private String fechaAceptacion;
    private String numeroPartida;
    private String cliente;
    private String avance;
    private String estado;

    //Id usuario
    private String idUsuario;

    //Datos de la apertura y cierre
    private String idApertura;
    private String notasApertura;
    private String tiempoInicioApertura;
    private String tiempoFinApertura;

    private String idCierre;
    private String notasCierre;
    private String tiempoInicioCierre;
    private String tiempoFinCierre;

    private ArrayList<String> fracciones;

    public Contenedor() {
        fracciones = new ArrayList<>();

        notasApertura = "";
        tiempoInicioApertura = "";
        tiempoFinApertura = "";

        notasCierre = "";
        tiempoInicioCierre = "";
        tiempoFinCierre = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaAceptacion() {
        return fechaAceptacion;
    }

    public void setFechaAceptacion(String fechaAceptacion) {
        this.fechaAceptacion = fechaAceptacion;
    }

    public String getNumeroPartida() {
        return numeroPartida;
    }

    public void setNumeroPartida(String numeroPartida) {
        this.numeroPartida = numeroPartida;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getAvance() {
        return avance;
    }

    public void setAvance(String avance) {
        this.avance = avance;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdApertura() {
        return idApertura;
    }

    public void setIdApertura(String idApertura) {
        this.idApertura = idApertura;
    }

    public String getNotasApertura() {
        return notasApertura;
    }

    public void setNotasApertura(String notasApertura) {
        this.notasApertura = notasApertura;
    }

    public String getTiempoInicioApertura() {
        return tiempoInicioApertura;
    }

    public void setTiempoInicioApertura(String tiempoInicioApertura) {
        this.tiempoInicioApertura = tiempoInicioApertura;
    }

    public String getTiempoFinApertura() {
        return tiempoFinApertura;
    }

    public void setTiempoFinApertura(String tiempoFinApertura) {
        this.tiempoFinApertura = tiempoFinApertura;
    }

    public String getIdCierre() {
        return idCierre;
    }

    public void setIdCierre(String idCierre) {
        this.idCierre = idCierre;
    }

    public String getNotasCierre() {
        return notasCierre;
    }

    public void setNotasCierre(String notasCierre) {
        this.notasCierre = notasCierre;
    }

    public String getTiempoInicioCierre() {
        return tiempoInicioCierre;
    }

    public void setTiempoInicioCierre(String tiempoInicioCierre) {
        this.tiempoInicioCierre = tiempoInicioCierre;
    }

    public String getTiempoFinCierre() {
        return tiempoFinCierre;
    }

    public void setTiempoFinCierre(String tiempoFinCierre) {
        this.tiempoFinCierre = tiempoFinCierre;
    }

    public boolean isEnviado() {
        return estado.equals("Enviado") || estado.equals("Terminado");
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public ArrayList<String> getFracciones() {
        return fracciones;
    }

    public void setFracciones(ArrayList<String> fracciones) {
        this.fracciones = fracciones;
    }

    public static ArrayList<Contenedor> dameListadoContenedor(String listJson) throws JSONException {
        ArrayList<Contenedor> contenedores = new ArrayList<>();

        JSONObject json = new JSONObject(listJson);
        JSONArray jsonArrayContenedores = json.getJSONArray("contenedores");

        for (int contador = 0; contador < jsonArrayContenedores.length(); contador++)
            contenedores.add(dameContenedor(jsonArrayContenedores.getJSONObject(contador)));

        return contenedores;
    }

    public static Contenedor dameContenedor(JSONObject objeto) throws JSONException {
        Contenedor contenedor = new Contenedor();

        contenedor.setId(objeto.getString("id"));
        contenedor.setEstado(objeto.getString("estado"));
        contenedor.setNumero(objeto.getString("numero"));
        contenedor.setNumeroReferencia(objeto.getString("numero_referencia"));
        contenedor.setCliente(objeto.getString("cliente"));
        contenedor.setFechaCreacion(objeto.getString("fecha_creacion"));
        contenedor.setNumeroPartida(objeto.getString("numero_partidas"));
        contenedor.setIdApertura(objeto.getString("id_apertura"));
        contenedor.setIdCierre(objeto.getString("id_cierre"));

        return contenedor;

    }

    public void guardarApertura(Context contexto) {
        Tools.guardarDatos(contexto, getIdApertura() + "/nota", getNotasApertura());
        Tools.guardarDatos(contexto, getIdApertura() + "/fecha-inicio", getTiempoInicioApertura());
        Tools.guardarDatos(contexto, getIdApertura() + "/fecha-fin", getTiempoFinApertura());
    }

    public void obtenerApertura(Context contexto) {
        setNotasApertura(Tools.obtenerDatosVacio(contexto, getIdApertura() + "/nota"));
        setTiempoInicioApertura(Tools.obtenerDatosVacio(contexto, getIdApertura() + "/fecha-inicio"));
        setTiempoFinApertura(Tools.obtenerDatosVacio(contexto, getIdApertura() + "/fecha-fin"));
    }

    public void guardarCierre(Context contexto) {
        Tools.guardarDatos(contexto, getIdCierre() + "/nota", getNotasCierre());
        Tools.guardarDatos(contexto, getIdCierre() + "/fecha-inicio", getTiempoInicioCierre());
        Tools.guardarDatos(contexto, getIdCierre() + "/fecha-fin", getTiempoFinCierre());
    }

    public void obtenerCierre(Context contexto) {
        setNotasCierre(Tools.obtenerDatosVacio(contexto, getIdCierre() + "/nota"));
        setTiempoInicioCierre(Tools.obtenerDatosVacio(contexto, getIdCierre() + "/fecha-inicio"));
        setTiempoFinCierre(Tools.obtenerDatosVacio(contexto, getIdCierre() + "/fecha-fin"));
    }

    public void guardarFracciones(Context contexto) {
        StringBuilder sb = new StringBuilder();
        for (String s : fracciones) {
            sb.append(s);
            sb.append(",");
        }

        Tools.guardarDatos(contexto, getId() + "/id", sb.toString());
    }

    public void obtenerFraccciones(Context contexto) {
        ArrayList<String> listadoFracciones = new ArrayList<>();
        String cadena = Tools.obtenerDatosVacio(contexto, getId() + "/id");
        for (String cadenaApoyo : cadena.split(",")) {
            listadoFracciones.add(cadenaApoyo);
        }

        System.out.println("listado de fracciones recuperadas");
        System.out.println(listadoFracciones.size());
        System.out.println(listadoFracciones);

        setFracciones(listadoFracciones);
    }

    public boolean esInicioApertura() {
        return getTiempoInicioApertura().equals("");
    }

    public boolean esInicioCierre() {
        return getTiempoInicioCierre().equals("");
    }

    public ArrayList<Fraccion> dameList() {
        ArrayList<Fraccion> listadoFracciones = new ArrayList<>();
        for (String cadenaApoyo : fracciones) {
            Fraccion frac = Globales.dameFraccion(cadenaApoyo);
            if (frac != null)
                listadoFracciones.add(frac);
            else {
                frac = new Fraccion();
                frac.setId(cadenaApoyo);
                listadoFracciones.add(frac);
            }
        }
        return listadoFracciones;
    }

    public boolean listoEnvio() {
        return !esInicioCierre() && !esInicioApertura();
    }

    @Override
    public String toString() {
        return "Contenedor{" +
                "id='" + id + '\'' +
                ", numero='" + numero + '\'' +
                ", numeroReferencia='" + numeroReferencia + '\'' +
                ", fechaCreacion='" + fechaCreacion + '\'' +
                ", fechaAceptacion='" + fechaAceptacion + '\'' +
                ", numeroPartida='" + numeroPartida + '\'' +
                ", cliente='" + cliente + '\'' +
                ", avance='" + avance + '\'' +
                ", estado='" + estado + '\'' +
                ", idUsuario='" + idUsuario + '\'' +
                ", idApertura='" + idApertura + '\'' +
                ", notasApertura='" + notasApertura + '\'' +
                ", tiempoInicioApertura='" + tiempoInicioApertura + '\'' +
                ", tiempoFinApertura='" + tiempoFinApertura + '\'' +
                ", idCierre='" + idCierre + '\'' +
                ", notasCierre='" + notasCierre + '\'' +
                ", tiempoInicioCierre='" + tiempoInicioCierre + '\'' +
                ", tiempoFinCierre='" + tiempoFinCierre + '\'' +
                ", numero_partidas calc='" + getFracciones().size() + '\'' +
                '}';
    }

    @Override
    public int compare(Contenedor lhs, Contenedor rhs) {
        //iguales
        if (lhs.getEstado().equals(rhs.getEstado()))
            return lhs.getNumero().compareToIgnoreCase(rhs.getNumero());

        // Caso que sean aceptados
        if (lhs.getEstado().equals("Aceptado")) return -1;
        if (rhs.getEstado().equals("Aceptado")) return 1;

        //Casos finales
        if (lhs.isEnviado()) return 1;
        if (rhs.isEnviado()) return -1;

        return 0;
    }
}
