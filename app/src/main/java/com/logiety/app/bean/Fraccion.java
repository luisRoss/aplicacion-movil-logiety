package com.logiety.app.bean;


import android.content.Context;
import android.support.annotation.NonNull;

import com.logiety.app.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Fraccion implements Serializable, Comparable<Fraccion> {
    private String id;
    private String descripcion;
    private String numero_parte;
    private String notas;
    private String id_usuario;
    private String fechaApertura;
    private String fechaCierre;
    private String imagenes;

    public Fraccion() {
    }

    public Fraccion(String id, String notas, String fechaApertura, String fechaCierre) {
        this.id = id;
        this.notas = notas;
        this.fechaApertura = fechaApertura;
        this.fechaCierre = fechaCierre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotas() {
        if (notas == null) return "";
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNumero_parte() {
        return numero_parte;
    }

    public void setNumero_parte(String numero_parte) {
        this.numero_parte = numero_parte;
    }

    public String getImagenes() {
        return imagenes;
    }

    public void setImagenes(String imagenes) {
        this.imagenes = imagenes;
    }

    public static ArrayList<Fraccion> dameListFraccionJson(JSONArray jsonArrayFracciones) {
        ArrayList<Fraccion> fracciones = new ArrayList<>();

        try {
            for (int contadorPartidas = 0; contadorPartidas < jsonArrayFracciones.length(); contadorPartidas++) {
                JSONObject objetoFraccion = null;
                objetoFraccion = jsonArrayFracciones.getJSONObject(contadorPartidas);
                Fraccion fraccion = new Fraccion();
                fraccion.setId(objetoFraccion.getString("id"));
                //fraccion.setId_usuario(cadenaId);
                fraccion.setNotas(objetoFraccion.getString("notas"));
                fraccion.setDescripcion(objetoFraccion.getString("descripcion"));
                fraccion.setNumero_parte(objetoFraccion.getString("numero_parte"));
                fraccion.setFechaApertura(objetoFraccion.getString("fecha_apertura"));
                fraccion.setFechaCierre(objetoFraccion.getString("fecha_cierre"));

                fracciones.add(fraccion);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fracciones;
    }

    public void guardar(Context contexto) {
        Tools.guardarDatos(contexto, getId() + "/nota", getNotas());
        Tools.guardarDatos(contexto, getId() + "/fecha-inicio", getFechaApertura());
        Tools.guardarDatos(contexto, getId() + "/fecha-fin", getFechaCierre());
        Tools.guardarDatos(contexto, getId() + "/imagenes", getImagenes());
    }

    public void obtener(Context contexto) {
        setNotas(Tools.obtenerDatosVacio(contexto, getId() + "/nota"));
        setFechaApertura(Tools.obtenerDatosVacio(contexto, getId() + "/fecha-inicio"));
        setFechaCierre(Tools.obtenerDatosVacio(contexto, getId() + "/fecha-fin"));
        setImagenes(Tools.obtenerDatosVacio(contexto, getId() + "/imagenes"));
    }

    public boolean conImagenes() {
        return !getImagenes().equals("");
    }

    public boolean esInicio() {
        return getFechaApertura().equals("");
    }

    public boolean esFin() {
        return !getFechaCierre().equals("");
    }

    public boolean listoEnvio() {
        return !esInicio() && esFin() && conImagenes();
    }


    @Override
    public String toString() {
        return "Fraccion{" +
                "id='" + id + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", numero_parte='" + numero_parte + '\'' +
                ", notas='" + notas + '\'' +
                ", id_usuario='" + id_usuario + '\'' +
                ", fechaApertura='" + fechaApertura + '\'' +
                ", fechaCierre='" + fechaCierre + '\'' +
                ", imagenes='" + imagenes + '\'' +
                '}';
    }

    @Override
    public int compareTo(@NonNull Fraccion another) {
        boolean listo = this.listoEnvio();
        boolean listoDos = another.listoEnvio();

        if (listo == listoDos) {
            return this.getDescripcion().compareToIgnoreCase(another.getDescripcion());

        } else {
            if (listo)
                return 1;
            else
                return -1;
        }


    }
}


