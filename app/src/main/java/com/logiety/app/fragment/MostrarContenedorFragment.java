package com.logiety.app.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logiety.app.ActivityMostrarPerfilContenedor;
import com.logiety.app.R;
import com.logiety.app.adapter.ContenedorGridAdapter;
import com.logiety.app.asynctask.AceptarContenedorAsyncTask;
import com.logiety.app.bean.Contenedor;
import com.logiety.app.utils.Globales;
import com.logiety.app.utils.Tools;

import java.util.List;

public class MostrarContenedorFragment extends Fragment {

    private View view;

    private RecyclerView recyclerView;
    public List<Contenedor> contenedores;
    public ContenedorGridAdapter mAdapter;
    private MostrarContenedorFragment fragmento;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.page_fragment_group, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        // use a linear layout manager
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), Tools.getGridSpanCount(getActivity())));
        recyclerView.setHasFixedSize(true);

        fragmento = this;

        // specify an adapter (see also next example)
        contenedores = Globales.dameLista();
        mAdapter = new ContenedorGridAdapter(getActivity(), contenedores);
        recyclerView.setAdapter(mAdapter);
        registerForContextMenu(recyclerView);

        mAdapter.setOnItemClickListener(new ContenedorGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, final Contenedor obj, int position) {

                if (obj.getEstado().equals("Pendiente")) {
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Aceptar contenedor")
                            .setMessage("¿ Deseas aceptar el contenedor ?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String cadena = Tools.obtenerDatos(getContext(), "cadenaConexion");
                                    new AceptarContenedorAsyncTask(fragmento, view, getContext(), cadena, obj).execute();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .show();

                } else {
                    String cadena = Tools.obtenerDatos(getContext(), "cadenaConexion");
                    new AceptarContenedorAsyncTask(fragmento, view, getContext(), cadena, obj).execute();
                }

            }
        });

        mAdapter.setmOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                getActivity().openContextMenu(view);
                return false;
            }
        });

        return view;
    }

    public void actualizarLista() {
        if (mAdapter != null) {
            mAdapter.actualizaContenedores();
        } else {
            System.out.println("adapter es nulo");
        }
    }

    public void irPerfilContenedor(Contenedor contenedor) {
        Intent intent = new Intent(getActivity(), ActivityMostrarPerfilContenedor.class);
        intent.putExtra("id", contenedor.getId());
        startActivity(intent);
    }
}
