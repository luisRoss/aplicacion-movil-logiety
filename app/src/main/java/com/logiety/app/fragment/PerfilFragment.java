package com.logiety.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.logiety.app.R;
import com.logiety.app.bean.Perfil;
import com.logiety.app.utils.Globales;


public class PerfilFragment extends Fragment {
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.page_fragment_setting, container, false);
        actualizar();
        return view;
    }

    public void actualizar() {
        Perfil perfil = Globales.damePerfil();

        TextView textApoyo = (TextView) view.findViewById(R.id.perfil_nombre_header);
        textApoyo.setText(perfil.getNombreCompleto());

        textApoyo = (TextView) view.findViewById(R.id.perfil_empresa);
        textApoyo.setText(perfil.getEmpresa());

        textApoyo = (TextView) view.findViewById(R.id.perfil_nombre);
        textApoyo.setText(perfil.getNombreCompleto());

        textApoyo = (TextView) view.findViewById(R.id.perfil_contenedores_enviados);
        textApoyo.setText(perfil.getContenedoresEnviados());

        textApoyo = (TextView) view.findViewById(R.id.perfil_contenedores_pendientes);
        textApoyo.setText("0");

        textApoyo = (TextView) view.findViewById(R.id.perfil_imagenes_enviadas);
        textApoyo.setText(perfil.getImagenesEnviadas());

        textApoyo = (TextView) view.findViewById(R.id.perfil_nombre);
        textApoyo.setText(perfil.getNombreCompleto());

        textApoyo = (TextView) view.findViewById(R.id.perfil_nombre_usuario);
        textApoyo.setText(perfil.getNombreUsuario());

        textApoyo = (TextView) view.findViewById(R.id.perfil_numero_gafete);
        textApoyo.setText(perfil.getGafete());

        textApoyo = (TextView) view.findViewById(R.id.perfil_correo_electronico);
        textApoyo.setText(perfil.getCorreoElectronico());

        textApoyo = (TextView) view.findViewById(R.id.perfil_ultimo_contenedor_enviado);
        textApoyo.setText(perfil.getUltimoContenedorEnviado());

        textApoyo = (TextView) view.findViewById(R.id.perfil_usuario_desde);
        textApoyo.setText(perfil.getFechaCreacion());

    }
}
