package com.logiety.app.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.media.ExifInterface;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.logiety.app.R;
import com.logiety.app.bean.Contenedor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class Tools {
    public static JSONObject enviarPeticionGET(String cadenaUrl, HashMap<String, String> parametros) throws IOException, JSONException {
        JSONObject respuesta = null;

        //Se cargan los parámetros
        Uri.Builder builder = new Uri.Builder();
        for (Map.Entry<String, String> entrada : parametros.entrySet())
            builder.appendQueryParameter(entrada.getKey(), entrada.getValue());

        //Se establece la conexión
        URL url = new URL(cadenaUrl + "?" + builder.build().getEncodedQuery());
        HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
        conexion.setRequestMethod("GET");
        conexion.setConnectTimeout(500);
        conexion.connect();

        int statusCode = conexion.getResponseCode();
        if (statusCode == HttpsURLConnection.HTTP_OK) {

            InputStream in = new BufferedInputStream(conexion.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String webPage = "", data = "";

            while ((data = reader.readLine()) != null)
                webPage += data + "\n";


            respuesta = new JSONObject(webPage);


        } else {
            throw new IOException("No se recibió el mensaje esperado");
        }

        return respuesta;
    }

    public static JSONObject enviarPeticionPOST(String cadenaUrl, HashMap<String, String> parametros) throws IOException, JSONException {
        JSONObject respuesta = null;

        //Se establece la conexión
        URL url = new URL(cadenaUrl);
        HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
        conexion.setRequestMethod("POST");
        conexion.setConnectTimeout(500);
        conexion.connect();

        //Se cargan los parámetros
        Uri.Builder builder = new Uri.Builder();
        for (Map.Entry<String, String> entrada : parametros.entrySet())
            builder.appendQueryParameter(entrada.getKey(), entrada.getValue());

        String query = builder.build().getEncodedQuery();

        OutputStream os = conexion.getOutputStream();
        os.write(query.getBytes("UTF-8"));
        os.close();

        int statusCode = conexion.getResponseCode();
        if (statusCode == HttpsURLConnection.HTTP_OK) {

            InputStream in = new BufferedInputStream(conexion.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String webPage = "", data = "";

            while ((data = reader.readLine()) != null)
                webPage += data + "\n";

            respuesta = new JSONObject(webPage);

        } else {
            throw new IOException("No se recibió el mensaje esperado");
        }

        return respuesta;
    }

    public static String dameRutaReal(ContentResolver resolver, Uri uri) {
        String rutaReal = "";

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = resolver.query(uri, filePathColumn, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            rutaReal = cursor.getString(columnIndex);

        } else {
            System.out.println("No se encontró la ruta para " + uri);
        }

        cursor.close();
        return rutaReal;
    }

    public static void guardarDatos(Context contexto, String llave, String cadena) {
        SharedPreferences preferencias = contexto.getSharedPreferences("Logiety", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString(llave, cadena);
        editor.apply();
    }

    public static void guardarContenedores(Context contexto, String llave, ArrayList<Contenedor> objeto) {
        SharedPreferences preferencias = contexto.getSharedPreferences("Logiety", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();

        Gson gson = new Gson();
        String cadena = gson.toJson(objeto);
        editor.putString(llave, cadena);
        editor.apply();
    }

    public static ArrayList<Contenedor> obtenerContenedores(Context contexto, String llave) {
        SharedPreferences preferencias = contexto.getSharedPreferences("Logiety", Context.MODE_PRIVATE);
        String cadena = preferencias.getString(llave, null);

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Contenedor>>() {
        }.getType();
        ArrayList<Contenedor> contenedores = gson.fromJson(cadena, type);
        return contenedores;
    }


    public static String obtenerDatosVacio(Context contexto, String llave) {
        String resultado = obtenerDatos(contexto, llave);
        if (resultado == null)
            return "";
        return resultado;
    }

    public static String obtenerDatos(Context contexto, String llave) {
        SharedPreferences preferencias = contexto.getSharedPreferences("Logiety", Context.MODE_PRIVATE);
        return preferencias.getString(llave, null);
    }

    public static String decodeBitmapOrientation(Context contexto, Uri uri) throws IOException {
        Bitmap bit = decodeSampledBitmap(contexto, uri);
        return bit.getHeight() < bit.getWidth() ? "Horizontal" : "Vertical";
    }


    /**
     * Se utiliza para enviar la imagen
     */
    public static InputStream decodeUriSend(Context c, Uri uri) {
        try {
            Bitmap bit = decodeSampledBitmap(c, uri);

            double tamanio = 1;
            while (!(bit.getHeight() / tamanio < 700 && bit.getWidth() / tamanio < 700)) {
                tamanio += 0.25;
            }

            System.out.println(tamanio);
            System.out.println(bit.getHeight() / tamanio + " " + bit.getWidth() / tamanio);

            bit = Bitmap.createScaledBitmap(bit, (int) (bit.getWidth() / tamanio),
                    (int) (bit.getHeight() / tamanio), true);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bit.compress(Bitmap.CompressFormat.PNG, 100, bos);
            return new ByteArrayInputStream(bos.toByteArray());

        } catch (Exception err) {
            err.printStackTrace();
        }

        return null;
    }

    public static Bitmap decodeMiniUri(Context c, Uri uri) throws IOException {
        Bitmap bit = decodeSampledBitmap(c, uri);
        bit = cropCenter(bit);
        bit = Bitmap.createScaledBitmap(bit, 100, 100, true);
        return bit;
    }

    private static Bitmap cropCenter(Bitmap bit) {
        if (bit.getWidth() >= bit.getHeight())
            return Bitmap.createBitmap(bit, bit.getWidth() / 2 - bit.getHeight() / 2,
                    0, bit.getHeight(), bit.getHeight());

        return Bitmap.createBitmap(bit, 0, bit.getHeight() / 2 - bit.getWidth() / 2,
                bit.getWidth(), bit.getWidth());
    }

    private static Bitmap decodeSampledBitmap(Context contexto, Uri uri) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        InputStream ims = contexto.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(ims, null, options);
        contexto.getContentResolver().notifyChange(uri, null);

        return modifyOrientation(bitmap, contexto.getContentResolver().openInputStream(uri));
    }

    private static Bitmap modifyOrientation(Bitmap bitmap, InputStream input) throws IOException {
        ExifInterface exif = new ExifInterface(input);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    private static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }


    public static Uri guardarArchivo(Uri sourceuri, Context contexto, File storageDir) {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        Uri imagenUri = null;

        try {

            InputStream ims = contexto.getContentResolver().openInputStream(sourceuri);
            String destinationFilename = null;
            File archivo = createImageFile(storageDir);
            destinationFilename = archivo.getAbsolutePath();


            bis = new BufferedInputStream(ims);
            bos = new BufferedOutputStream(new FileOutputStream(destinationFilename, false));
            byte[] buf = new byte[1024];
            bis.read(buf);
            do {
                bos.write(buf);
            } while (bis.read(buf) != -1);
            imagenUri = FileProvider.getUriForFile(contexto, "com.example.android.fileprovider", archivo);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return imagenUri;
    }

    public static File createImageFile(File storageDir) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        return image;
    }


    private static float getAPIVerison() {

        Float f = null;
        try {
            StringBuilder strBuild = new StringBuilder();
            strBuild.append(android.os.Build.VERSION.RELEASE.substring(0, 2));
            f = new Float(strBuild.toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return f.floatValue();
    }

    public static void systemBarLolipop(Activity act) {
        if (getAPIVerison() >= 5.0) {
            Window window = act.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(act.getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    public static void systemBarLolipopDark(Activity act) {
        if (getAPIVerison() >= 5.0) {
            Window window = act.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    public static int getGridExplorerCount(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        float screenWidth = displayMetrics.widthPixels;
        float cellWidth = activity.getResources().getDimension(R.dimen.explorer_item_size);
        return Math.round(screenWidth / cellWidth);
    }

    public static int getGridSpanCount(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        float screenWidth = displayMetrics.widthPixels;
        float cellWidth = activity.getResources().getDimension(R.dimen.recycler_item_size);
        return Math.round(screenWidth / cellWidth);
    }
}
