package com.logiety.app.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {
    public static String dameFecha() {
        Calendar calendario = GregorianCalendar.getInstance();
        Date fecha = calendario.getTime();

        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd-MM-yyyy");
        System.out.println(formatoDeFecha.format(fecha));
        return formatoDeFecha.format(fecha);
    }
}
