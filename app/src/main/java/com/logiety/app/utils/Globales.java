package com.logiety.app.utils;

import com.logiety.app.bean.Contenedor;
import com.logiety.app.bean.Fraccion;
import com.logiety.app.bean.Perfil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Globales {
    public static boolean DEBUG = false;

    public static int VERSION_DATABASE = 16;
    public static String DATABASE_NAME = "LogietyPrueba";

    public static String protocolo = "https";

    public static String dameIp() {
        return DEBUG ? "192.168.1.65:8000" : "logiety.mybluemix.net";
    }

    public static String damePuerto() {
        return DEBUG ? "8000" : "80";
    }

    public static String dameRutaAcceso() {
        return protocolo + "://" + dameIp() + "/inicio-sesion-remoto/";
    }

    public static String dameSolicitarDatosPerfil() {
        return protocolo + "://" + dameIp() + "/solicitar-datos-perfil-remoto/";
    }

    public static String dameRutaIngresarContenedoresInicio() {
        return protocolo + "://" + dameIp() + "/generar-contenedor-remoto-inicio/";
    }

    public static String dameRutaIngresarContenedoresImagenes() {
        return protocolo + "://" + dameIp() + "/generar-contenedor-remoto-imagenes/";
    }

    public static String dameRutaIngresarContenedoresCierre() {
        return protocolo + "://" + dameIp() + "/generar-contenedor-remoto-cierre/";
    }

    public static String dameObtenerContenedor() {
        return protocolo + "://" + dameIp() + "/obtener-contenedor/";
    }

    public static String dameLiberarContenedor() {
        return protocolo + "://" + dameIp() + "/liberar-contenedor/";
    }

    private static HashMap<String, Contenedor> contenedores = new HashMap<>();

    public static ArrayList<Contenedor> dameLista() {
        ArrayList<Contenedor> listado = new ArrayList<>();
        for (Map.Entry<String, Contenedor> entrada : contenedores.entrySet())
            listado.add(entrada.getValue());

        return listado;
    }

    public static void cargarInformacion(ArrayList<Contenedor> contenedoresLista) {
        contenedores.clear();
        for (Contenedor cont : contenedoresLista)
            contenedores.put(cont.getId(), cont);
    }

    public static Contenedor dameContenedor(String id) {
        return contenedores.get(id);
    }


    private static HashMap<String, Fraccion> fracciones = new HashMap<>();

    public static Fraccion dameFraccion(String cadena) {
        return fracciones.get(cadena);
    }

    public static Contenedor cargarFracciones(ArrayList<Fraccion> fraccionesA, String contenedor) {
        for (Fraccion frac : fraccionesA)
            fracciones.put(frac.getId(), frac);

        //Se cargan las fracciones en el contenedor
        Contenedor con = dameContenedor(contenedor);
        ArrayList<String> listado = new ArrayList<>();
        for (Fraccion frac : fraccionesA)
            listado.add(frac.getId());
        con.setFracciones(listado);

        return con;
    }

    private static Perfil perfil;

    public static Perfil damePerfil() {
        if (perfil == null) return new Perfil();
        return perfil;
    }

    public static void setPerfil(Perfil perfilA) {
        perfil = perfilA;
    }


}
