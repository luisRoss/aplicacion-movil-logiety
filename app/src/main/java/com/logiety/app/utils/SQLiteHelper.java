package com.logiety.app.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static String consultaCreacionContenedor =
            "CREATE TABLE Contenedor (id text PRIMARY KEY, avance text, fecha_aceptacion text," +
                    " id_usuario text, id_apertura text, nota_apertura text, tiempo_inicio_apertura text," +
                    "tiempo_fin_apertura text, id_cierre text, nota_cierre text, tiempo_inicio_cierre text," +
                    " tiempo_fin_cierre text)";

    public static String consultaCreacionFraccion =
            "CREATE TABLE Fraccion (id text PRIMARY KEY, notas text, fecha_apertura text," +
                    "fecha_cierre text, id_usuario text)";

    public static String consultaCreacionImagen =
            "CREATE TABLE Imagen (id integer PRIMARY KEY AUTOINCREMENT, id_fraccion text,ruta text)";

    public SQLiteHelper(Context contexto, String nombre, CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase baseDatos) {
        baseDatos.execSQL(consultaCreacionContenedor);
        baseDatos.execSQL(consultaCreacionFraccion);
        baseDatos.execSQL(consultaCreacionImagen);
    }

    @Override
    public void onUpgrade(SQLiteDatabase baseDatos, int versionA, int versionN) {
        baseDatos.execSQL("drop table Contenedor");
        baseDatos.execSQL("drop table Fraccion");
        baseDatos.execSQL("drop table Imagen");
        baseDatos.execSQL(consultaCreacionContenedor);
        baseDatos.execSQL(consultaCreacionFraccion);
        baseDatos.execSQL(consultaCreacionImagen);
    }
}

