package com.logiety.app.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logiety.app.R;
import com.logiety.app.bean.Contenedor;
import com.logiety.app.utils.Globales;
import com.logiety.app.widget.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ContenedorGridAdapter extends RecyclerView.Adapter<ContenedorGridAdapter.ViewHolder> implements Filterable {

    private List<Contenedor> original_items = new ArrayList<>();
    private List<Contenedor> filtered_items = new ArrayList<>();
    private ItemFilter mFilter = new ItemFilter();
    private Contenedor objetoContenedor;

    private Context ctx;


    // for item click listener
    private OnItemClickListener mOnItemClickListener;
    private View.OnLongClickListener mOnLongClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Contenedor obj, int position);
    }

    public Contenedor getIdContenedorSeleccionado() {
        return objetoContenedor;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setmOnLongClickListener(final View.OnLongClickListener item) {
        this.mOnLongClickListener = item;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView numero_contenedor;
        public TextView numero_sello;
        public TextView fecha;
        public TextView estado;
        public TextView empresa;
        public TextView partidas;
        public ImageView image;
        public LinearLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);

            numero_contenedor = (TextView) v.findViewById(R.id.numero_contenedor);
            numero_sello = (TextView) v.findViewById(R.id.numero_sello);
            fecha = (TextView) v.findViewById(R.id.fecha_creacion);
            estado = (TextView) v.findViewById(R.id.estado);
            empresa = (TextView) v.findViewById(R.id.empresa);
            partidas = (TextView) v.findViewById(R.id.numero_partidas);
            image = (ImageView) v.findViewById(R.id.image);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }

    }

    public Filter getFilter() {
        return mFilter;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ContenedorGridAdapter(Context ctx, List<Contenedor> items) {
        this.ctx = ctx;
        Collections.sort(items, new Contenedor());
        original_items = items;
        filtered_items = items;
    }

    @Override
    public ContenedorGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_groups, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Contenedor g = filtered_items.get(position);

        holder.numero_contenedor.setText(g.getNumero());
        holder.numero_sello.setText(g.getNumeroReferencia());

        holder.partidas.setText(String.valueOf(g.getNumeroPartida()));
        holder.fecha.setText(g.getFechaCreacion());

        String estado = "";
        System.out.println("estado");
        System.out.println(g.getEstado());
        if (g.isEnviado())
            estado = "Enviado";
        else if (g.getEstado().equals("Pendiente"))
            estado = "Pendiente";
        else estado = "Aceptado";

        holder.estado.setText(estado);

        Resources res = holder.estado.getContext().getResources();
        Shader textShader = new LinearGradient(0, 0, 20, 20, new int[]{
                Color.WHITE, res.getColor(R.color.exito_contenedor), res.getColor(R.color.pendiente_contenedor), Color.WHITE
        }, new float[]{0.25f, 0.50f, 0.75f, 1}, Shader.TileMode.CLAMP);
        holder.estado.getPaint().setShader(textShader);

        if (g.isEnviado())
            holder.estado.setBackgroundColor(holder.estado.getContext().getResources().getColor(R.color.exito_contenedor));
        else if (g.getEstado().equals("Aceptado"))
            holder.estado.setBackgroundColor(holder.estado.getContext().getResources().getColor(R.color.aceptado_contenedor));
        else {
            holder.estado.setBackgroundColor(holder.estado.getContext().getResources().getColor(R.color.pendiente_contenedor));
        }

        if (g.getCliente().length() > 7)
            holder.empresa.setText(g.getCliente().substring(0, 6) + "...");
        else
            holder.empresa.setText(g.getCliente());

        int icono = g.isEnviado() ? R.drawable.icono_enviado :
                (g.getEstado().equals("Aceptado")) ? R.drawable.icono_aceptado : R.drawable.icono_pendiente;
        Picasso.with(ctx).load(icono).resize(200, 200).transform(new CircleTransform()).into(holder.image);

        // view detail message conversation
        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, g, position);
                }
            }
        });

        holder.lyt_parent.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                if (mOnLongClickListener != null) {
                    objetoContenedor = g;
                    mOnLongClickListener.onLongClick(view);
                }
                return true;
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return 1;
    }

    public Contenedor dameContenedor(int elemento) {
        return original_items.get(elemento);
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            final List<Contenedor> list = original_items;
            final List<Contenedor> result_list = new ArrayList<>(list.size());

            results.values = result_list;
            results.count = result_list.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<Contenedor>) results.values;
            notifyDataSetChanged();
        }

    }

    public void clear() {
        int size = this.original_items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.original_items.remove(0);
            }

            //notifyDataSetChanged();
        }
    }

    public void actualizaContenedores() {
        clear();
        this.original_items = Globales.dameLista();
        Collections.sort(this.original_items, new Contenedor());
        this.filtered_items = Globales.dameLista();
        Collections.sort(this.filtered_items, new Contenedor());

        notifyDataSetChanged();
    }
}