package com.logiety.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logiety.app.R;
import com.logiety.app.bean.Fraccion;
import com.logiety.app.dao.FraccionDao;

import java.util.ArrayList;
import java.util.List;

public class PartidaListAdapter extends RecyclerView.Adapter<PartidaListAdapter.ViewHolder> {

    private List<Fraccion> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private View.OnLongClickListener mOnLongClick;
    private String idFraccion;

    public String getIdFraccionSeleccionado() {
        return idFraccion;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, Fraccion obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setmOnLongClick(View.OnLongClickListener mOnLongClick) {
        this.mOnLongClick = mOnLongClick;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PartidaListAdapter(Context context, List<Fraccion> items) {
        this.items = items;
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imagen;
        public TextView nombre;
        public TextView descripcion;
        public LinearLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.nombre_partida);
            descripcion = (TextView) v.findViewById(R.id.descripcion);
            imagen = (ImageView) v.findViewById(R.id.imagen_estado_partida);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    @Override
    public PartidaListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_partida, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Fraccion c = items.get(position);

        holder.nombre.setText(c.getDescripcion());
        holder.descripcion.setText(c.getNumero_parte());

        if (c.listoEnvio())
            holder.imagen.setImageResource(R.drawable.boton_terminado);

        //Picasso.with(ctx).load("").resize(100, 100).transform(new CircleTransform()).into(holder.imagen);

        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    idFraccion = c.getId();
                    mOnItemClickListener.onItemClick(view, c, position);
                }
            }
        });

        holder.lyt_parent.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                if (mOnItemClickListener != null) {
                    idFraccion = c.getId();
                    mOnItemClickListener.onItemClick(view, c, position);
                }
                return false;
            }
        });
    }

    public Fraccion getItem(int position) {
        return items.get(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

}