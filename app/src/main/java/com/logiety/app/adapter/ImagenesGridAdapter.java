package com.logiety.app.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.media.ExifInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.logiety.app.R;
import com.logiety.app.bean.Imagen;
import com.logiety.app.utils.Tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class ImagenesGridAdapter extends RecyclerView.Adapter<ImagenesGridAdapter.ViewHolder> {

    private List<Imagen> items = new ArrayList<>();
    private Context ctx;

    private OnItemClickListener mOnItemClickListener;
    private View.OnLongClickListener mOnLongClickListener;

    private int posicion;

    public interface OnItemClickListener {
        void onItemClick(View view, Imagen obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void setmOnLongClickListener(final View.OnLongClickListener item) {
        this.mOnLongClickListener = item;
    }

    public void eliminarSeleccionado() {
        items.remove(posicion);
        notifyItemRemoved(posicion);
        notifyItemRangeChanged(posicion, items.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView image;
        public MaterialRippleLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            lyt_parent = (MaterialRippleLayout) v.findViewById(R.id.lyt_parent);
        }

    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public ImagenesGridAdapter(Context ctx, List<Imagen> items) {
        this.ctx = ctx;
        this.items = items;
    }

    @Override
    public ImagenesGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_explore, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Imagen p = items.get(position);
        System.out.println("tamanio de items " + items.size());

        try {

            Uri myUri = Uri.parse(p.getRuta());
            holder.image.setImageBitmap(Tools.decodeMiniUri(ctx, myUri));
            holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("Simple click " + position);
                    posicion = position;
                    mOnItemClickListener.onItemClick(view, p, position);
                }
            });

            holder.lyt_parent.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View view) {
                    if (mOnLongClickListener != null) {
                        System.out.println("Se selecciona posicion por long click " + position);
                        posicion = position;
                        mOnLongClickListener.onLongClick(view);
                    }

                    return true;
                }
            });


        } catch (Exception e) {

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            e.printStackTrace();
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}